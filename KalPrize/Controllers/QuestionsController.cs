﻿using KalPrize.Infrastructure;
using KalPrize.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using unirest_net.http;

namespace KalPrize.Controllers
{
    public class QuestionsController : ApiController
    {
        public List<User> usersList;
        private const string phoenixUrl = "https://rest-sgs1.ott.kaltura.com";
        private const string appToken = "b279c314fb274ba3a20926d77b381ebf";
        private const string appId = "863436f20e774da8848cbc4b204c8a40";

        PhoenixWrapper phoenixWrapper = new PhoenixWrapper(phoenixUrl, 463, appToken, appId);
        public QuestionSet Get(int assetId)
        {
            QuestionSet qestionSet = new QuestionSet();
            qestionSet.questions = new List<Question>();
            Question q = new Question();
            switch (assetId)
            {
                case 312627:
                    //Who wins questions
                    //var assetData = phoenixWrapper.GetAssetData(assetId.ToString());
                    //string[] splitters = new string[] { "VS " };

                    //string[] teams = assetData.Description.Split(splitters, StringSplitOptions.RemoveEmptyEntries);
                    //string team1 = teams[0];
                    //string team2 = teams[1];
                    string team1 = "Arsenal";
                    string team2 = "Chelsea";
                    q = SportsQuestions.GetWhoWillWinQuestion(team1, team2);

                    qestionSet.questions.Add(q);
                    qestionSet.questionSetId = 1;
                    qestionSet.questionSetName = "Who Will Win";
                                        
                    break;
                case 2:
                    
                    break;
                case 3:
                    break;
                default:
                    q = TVQuestions.GetGameOfThrones3();
                    qestionSet.questions.Add(q);
                    q = TVQuestions.GetGameOfThrones1();
                    qestionSet.questions.Add(q);
                    q = TVQuestions.GetGameOfThrones2();
                    qestionSet.questions.Add(q);
                    qestionSet.questionSetId = 2;
                    qestionSet.questionSetName = "Game of Thrones Trivia";
                    break;
            }
            return qestionSet;
        }

        

        public void Post(int questionSetId, int answesId)
        {
            //call gameAdapter with actionType, extraParams


            //get awardResponse, part of it is the score
            // ADD user's score

        }
       
    }
}