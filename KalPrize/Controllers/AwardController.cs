﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using KalPrize.Infrastructure;
using KalPrize.Models;
using Kaltura;
using Kaltura.Types;

namespace KalPrize.Controllers
{
    public class AwardController : ApiController
    {
        private readonly PhoenixWrapper _PhoenixWrapper = new PhoenixWrapper("https://rest-sgs1.ott.kaltura.com", 463, "b279c314fb274ba3a20926d77b381ebf", "863436f20e774da8848cbc4b204c8a40");
        private readonly Dictionary<int, string> _Ranks = new Dictionary<int, string>{
            {0, "Warden"},
            {1000,"Lord Commander"},
            {2000,"Mother of dragons"},
            {3000,"Khaleesi"},
            {4000,"King of Westeros"},
        };
        private const string SCORE_KEY = "KalPrizeScore";
        private const string RANK_KEY = "KalPrizeRank";

        public AwardResponse Get(int userID, string actionType, bool isCorrect)
        {
            var response = new AwardResponse();
            var ottUser = _PhoenixWrapper.FindUserByID(userID);
            if (!ottUser.DynamicData.ContainsKey(SCORE_KEY)) ottUser.DynamicData[SCORE_KEY] = new StringValue { Value = "0" };
            if (!ottUser.DynamicData.ContainsKey(RANK_KEY)) ottUser.DynamicData[RANK_KEY] = new StringValue { Value = _Ranks[0] };

            var currentScore = int.Parse(ottUser.DynamicData[SCORE_KEY].Value);
            response.TotalKPoints = currentScore;
            response.CurrentRank = ottUser.DynamicData[RANK_KEY].Value;
            response.EarnedKPoints = isCorrect ? 100 : 0;
            response.TotalKPoints += response.EarnedKPoints;

            ottUser.DynamicData[SCORE_KEY] = new StringValue { Value = response.TotalKPoints.ToString() };
            var newRankKey = _Ranks.Keys.Where(rankPoints => currentScore >= rankPoints).Max();
            response.NewRank = _Ranks[newRankKey];

            ottUser.DynamicData[RANK_KEY] = new StringValue { Value = response.NewRank };

            _PhoenixWrapper.UpdatedOttUser(ottUser);

            return response;
        }
    }
}
