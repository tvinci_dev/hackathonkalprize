﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KalPrize.Models
{
    public class AwardResponse
    {
        public int TotalKPoints { get; set; }
        public int EarnedKPoints { get; set; }
        public string CurrentRank { get; set; }
        public string NewRank { get; set; }


    }
}