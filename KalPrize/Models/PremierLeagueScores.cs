﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KalPrize.Models
{
    public class PGoal
    {
        public string minute { get; set; }
        public string player { get; set; }
        public bool isRightFoot { get; set; }
        public bool? isHead { get; set; }
    }

    public class PTeam1
    {
        public string teamName { get; set; }
        public List<PGoal> goals { get; set; }
    }

    public class PGoal2
    {
        public string minute { get; set; }
        public string player { get; set; }
        public bool isRightFoot { get; set; }
    }

    public class PTeam2
    {
        public string teamName { get; set; }
        public List<PGoal2> goals { get; set; }
    }

    public class PremierLeagueScores
    {
        public PTeam1 team1 { get; set; }
        public PTeam2 team2 { get; set; }
    }
}