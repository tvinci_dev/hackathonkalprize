﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using unirest_net.http;

namespace KalPrize.Models
{
    public static class SportsQuestions
    {
        public static Question GetWhoWillWinQuestion(string team1, string team2)
        {
            Question q = new Question();
            q.questionID = new Random().Next();
            q.questionText = "Guess who will win: " + team1 + " or " + team2 + " ?";
            q.answers = new List<Answer>();
            q.answers.Add(new Answer()
            {
                answerID = 1,
                answerText = team1,
                answerImgaeUrl = "http://www.talesfromthetopflight.com/wp-content/uploads/2017/01/arsenal.png",
            });
            q.answers.Add(new Answer()
            {
                answerID = 2,
                answerText = team2,
                answerImgaeUrl = "http://c8.alamy.com/comp/HHF1W6/chelsea-fc-logo-icon-button-HHF1W6.jpg",
            });
            return q;
        }

        public static string GetPremierLeagueScores(string team1, string team2)
        {
            // These code snippets use an open-source library. http://unirest.io/net
            HttpResponse<PremierLeagueScores> response = Unirest.get("https://heisenbug-premier-league-live-scores-v1.p.mashape.com/api/premierleague/scorers?team1={team1}&team2={team2}")
            .header("X-Mashape-Key", "nbeevjj9EcmshWbZDPseJcWkgO3lp18iyMEjsnIwTjZ2QdXpOZ")
            .header("Accept", "application/json")
            .asJson<PremierLeagueScores>();

            return CheckWhoWon(response.Body);
        }

        public static string GetLaLigaMatchScore(string team1, string team2)
        {
            string url = $"https://heisenbug-la-liga-live-scores-v1.p.mashape.com/api/laliga/scorers?team1={team1}&team2={team2}";
            HttpResponse<LaLigaScoreResponse> response = Unirest.get(url)
                        .header("X-Mashape-Key", "nbeevjj9EcmshWbZDPseJcWkgO3lp18iyMEjsnIwTjZ2QdXpOZ")
                        .header("Accept", "application/json")
                        .asJson<LaLigaScoreResponse>();

            return response.Body.team1.teamName;
        }
        private static string CheckWhoWon(PremierLeagueScores response)
        {
            int team1Goals = response.team1.goals.Count;
            int team2Goals = response.team2.goals.Count;

            if (team1Goals > team2Goals)
            {
                return response.team1.teamName;
            }
            if (team2Goals > team1Goals)
            {
                return response.team2.teamName;
            }
            else //tie
            {
                return "Tie";
            }
        }
    }
}