﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KalPrize.Models
{
    public class Answer
    {
        public int answerID { get; set; }
        public string answerText { get; set; }
        public string answerImgaeUrl { get; set; }

        
    }

    public class Question
    {
        public int questionID { get; set; }
        public string questionText { get; set; }
        public string questionImageUrl { get; set; }
        public List<Answer> answers { get; set; }
        public int correctAnswerId { get; set; }
    }

    public class QuestionSet
    {
        public int questionSetId { get; set; }
        public string questionSetName { get; set; }
        public List<Question> questions { get; set; }
    }
}