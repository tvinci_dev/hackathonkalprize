﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KalPrize.Models
{
    public static class TVQuestions
    {

        public static Question GetGameOfThrones1()
        {
            Question q = new Question()
            {
                questionID = new Random().Next(),
                questionText = "What is the name of Arya Stark's sword?",
                answers = new List<Answer>(),
                questionImageUrl = "",
                correctAnswerId = 2,
            };
            q.answers.Add(new Answer()
            {
                answerID = 1,
                answerImgaeUrl = "https://static1.squarespace.com/static/52fc05c9e4b08fc45bd99090/57016c3c1d07c0902336d748/57016c3d45bf2107c89cb9c3/1459711045253/MGoT_oberyns_spear_1_1200x800.jpg",
                answerText = "Spear"
            });
            q.answers.Add(new Answer()
            {
                answerID = 2,
                answerImgaeUrl = "http://www.thinkgeek.com/images/products/additional/large/kkso_got_needle_sword_det1.jpg",
                answerText = "Needle"
            });
            q.answers.Add(new Answer()
            {
                answerID = 3,
                answerImgaeUrl = "https://images1.laweekly.com/imager/u/745xauto/6807259/music1-1_4-14-16-3ae5df4823512af5.jpg",
                answerText = "Slash"
            });
            q.answers.Add(new Answer()
            {
                answerID = 4,
                answerImgaeUrl = "https://www.allworship.com/wp-content/uploads/2013/11/bigstock-Rose-thorns-15028199.jpg",
                answerText = "Thorn"
            });

            return q;
        }

        public static Question GetGameOfThrones2()
        {
            Question q = new Question()
            {
                questionID = new Random().Next(),
                questionText = "Daenerys Targaryen has three children. These 'children' are actually what?",
                answers = new List<Answer>(),
                questionImageUrl = "",
                correctAnswerId = 3,
            };
            q.answers.Add(new Answer()
            {
                answerID = 1,
                answerImgaeUrl = "https://cdn.shopify.com/s/files/1/0267/4223/products/Stabby-The-Unicorn-Pink-clean_800x.jpg?v=1510687035",
                answerText = "unicorns"
            });
            q.answers.Add(new Answer()
            {
                answerID = 2,
                answerImgaeUrl = "https://www.google.co.il/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&ved=0ahUKEwjM8ujrs7vYAhVIy6QKHRB4AEQQjBwIBA&url=http%3A%2F%2Ffaunaimage.com%2Fwp-content%2Fuploads%2F2017%2F09%2Fother-wolf-streching-wolves-wildlife-wild-predators-animals-nature-cute-sweet-funny-animal-wallpapers-for-ipad.jpg&psig=AOvVaw3PN__mN-F-OdHpyaQQ70Ik&ust=1515055534940386",
                answerText = "wolves"
            });
            q.answers.Add(new Answer()
            {
                answerID = 3,
                answerImgaeUrl = "http://www.telegraph.co.uk/content/dam/tv/2017/08/07/drogon_1_trans_NvBQzQNjv4BqfmWw8ZtkYoKgUQYLfpqhYv3Lyj9RShzlDBeus8lXwhc.jpg?imwidth=450",
                answerText = "dragons"
            });
            q.answers.Add(new Answer()
            {
                answerID = 4,
                answerImgaeUrl = "https://vignette.wikia.nocookie.net/disney/images/e/ed/TLK_Simba.png/revision/latest?cb=20120730040249",
                answerText = "lions"
            });

            return q;
        }

        public static Question GetGameOfThrones3()
        {
            Question q = new Question()
            {
                questionID = new Random().Next(),
                questionText = "Who is going to sit on the iron throne chair?",
                answers = new List<Answer>(),
                questionImageUrl = "",
                correctAnswerId = 4,
            };
            q.answers.Add(new Answer()
            {
                answerID = 1,
                answerImgaeUrl = "https://cdn.24.co.za/files/Cms/General/d/5464/db546cb65ddd49b8bb26307a0b593689.jpg",
                answerText = "Jon Snow"
            });
            q.answers.Add(new Answer()
            {
                answerID = 2,
                answerImgaeUrl = "https://www.unilad.co.uk/wp-content/uploads/2016/07/king1.jpg",
                answerText = "Night King"
            });
            q.answers.Add(new Answer()
            {
                answerID = 3,
                answerImgaeUrl = "http://cdn.newsapi.com.au/image/v1/928ffc271f4f43670b8b17462c2f9ec6",
                answerText = "khaleesi"
            });
            q.answers.Add(new Answer()
            {
                answerID = 4,
                answerImgaeUrl = "http://blog.mediander.com/wp-content/uploads/2016/09/RonYek2.jpg",
                answerText = "Ron Yekutiel"
            });

            return q;
        }

        public static Question GetNameActorSeries(string seriesName)
        {
            Question q = new Question()
            {
                questionID = new Random().Next(),
                questionText = "Who is the main actor in " + seriesName + " ?",
                answers = new List<Answer>(),
                questionImageUrl = "",
                correctAnswerId = 1,
            };
            q.answers.Add(new Answer()
            {
                answerID = 1,
                answerImgaeUrl = "https://images-na.ssl-images-amazon.com/images/M/MV5BOTRhZWVmMmQtYWNmNy00NjkwLWEzYWItOTk5ZjZiOTg1YTQzXkEyXkFqcGdeQXVyMjIyNzU0OA@@._V1_.jpg",
                answerText = "Seong Ji"
            });
            q.answers.Add(new Answer()
            {
                answerID = 2,
                answerImgaeUrl = "https://images-na.ssl-images-amazon.com/images/M/MV5BMjAxMTE2MzU4MF5BMl5BanBnXkFtZTgwNDQzNjcyMjE@._V1_SY1000_SX1500_AL_.jpg",
                answerText = "Ki-joon Uhm"
            });
            q.answers.Add(new Answer()
            {
                answerID = 3,
                answerImgaeUrl = "http://cdn-r1.unilad.co.uk/wp-content/uploads/2016/06/kim-jong-un-3.jpg",
                answerText = "kim jong un"
            });
            q.answers.Add(new Answer()
            {
                answerID = 4,
                answerImgaeUrl = "http://blog.mediander.com/wp-content/uploads/2016/09/RonYek2.jpg",
                answerText = "Ron Yekutiel"
            });

            return q;
        }

        public static Question GetSeriesStoryPlace(string seriesName)
        {
            Question q = new Question()
            {
                questionID = new Random().Next(),
                questionText = "Where does " + seriesName + " takes place?",
                answers = new List<Answer>(),
                questionImageUrl = "",
                correctAnswerId = 2,

            };
            q.answers.Add(new Answer()
            {
                answerID = 1,
                answerImgaeUrl = "",
                answerText = "London"
            });
            q.answers.Add(new Answer()
            {
                answerID = 2,
                answerImgaeUrl = "",
                answerText = "Seoul"
            });
            q.answers.Add(new Answer()
            {
                answerID = 3,
                answerImgaeUrl = "",
                answerText = "Paris"
            });
            q.answers.Add(new Answer()
            {
                answerID = 4,
                answerImgaeUrl = "",
                answerText = "Tel Aviv"
            });

            return q;
        }

        public static Question GetDeseas()
        {
            Question q = new Question()
            {
                questionID = new Random().Next(),
                questionText = "What kind of deseas the main actor suffers from?",
                answers = new List<Answer>(),
                questionImageUrl = "",
                correctAnswerId = 4,

            };
            q.answers.Add(new Answer()
            {
                answerID = 1,
                answerImgaeUrl = "",
                answerText = "Bad hair"
            });
            q.answers.Add(new Answer()
            {
                answerID = 2,
                answerImgaeUrl = "",
                answerText = "Flu"
            });
            q.answers.Add(new Answer()
            {
                answerID = 3,
                answerImgaeUrl = "",
                answerText = "Broken heart"
            });
            q.answers.Add(new Answer()
            {
                answerID = 4,
                answerImgaeUrl = "",
                answerText = "Amnesia"
            });

            return q;
        }
    }
}