﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KalPrize.Models
{
    public class PostResultResponse
    {
        public int UserId { get; set; }
        public bool IsCorrect { get; set; }
    }
}