﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KalPrize.Models
{
    public class Goal
    {
        public string minute { get; set; }
        public string player { get; set; }
        public bool isRightFoot { get; set; }
    }

    public class Team1
    {
        public string teamName { get; set; }
        public List<Goal> goals { get; set; }
    }

    public class Goal2
    {
        public string minute { get; set; }
        public string player { get; set; }
        public bool isLeftFoot { get; set; }
        public bool? isRightFoot { get; set; }
    }

    public class Team2
    {
        public string teamName { get; set; }
        public List<Goal2> goals { get; set; }
    }

    public class LaLigaScoreResponse
    {
        public Team1 team1 { get; set; }
        public Team2 team2 { get; set; }
    }
}