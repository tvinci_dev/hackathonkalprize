﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Kaltura;
using Kaltura.Services;
using Kaltura.Types;
using log4net;
using Kaltura.Enums;

namespace KalPrize.Infrastructure
{
    public class PhoenixWrapper
    {
        private readonly Kaltura.Client _PhoenixClient;
        private static readonly ILog _Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly int _GroupID;
        private readonly string _AppToken;
        private readonly string _AppID;

        public PhoenixWrapper(string phoenixServiceUrl, int groupID, string appToken, string appID)
        {
            _GroupID = groupID;
            _AppToken = appToken;
            _AppID = appID;

            _PhoenixClient = new Kaltura.Client(new Configuration
            {
                ServiceUrl = phoenixServiceUrl,
                Logger = new PhoenixLoggerForLog4Net(),
            });

            var ksToken = GetPhoenixToken();
            _PhoenixClient.setKS(ksToken);
        }

        public Asset GetAssetData(string assetId)
        {
            try
            {
                var assetResult = AssetService.Get(assetId, AssetReferenceType.MEDIA).ExecuteAndWaitForResponse(_PhoenixClient);
                return assetResult;
            }
            catch (Exception e)
            {
                _Logger.Error($"Error> GetAssetContext> Exception for assetId:{assetId}. Ex: {e.Message}");
                return null;
            }
        }

        public OTTUser FindUserByID(int userID)
        {
            var ottUserGetRequest = OttUserService.Get();
            ottUserGetRequest.UserId = userID;
            return ottUserGetRequest.ExecuteAndWaitForResponse(_PhoenixClient);
        }

        public OTTUser FindUserByExternalID(string externalID)
        {
            var filters = new OTTUserFilter { ExternalIdEqual = externalID };
            var foundUser = FindUsers(filters).FirstOrDefault(u => u.ExternalId.Equals(externalID, StringComparison.InvariantCultureIgnoreCase));
            return foundUser;
        }

        public OTTUser FindUserByUsername(string username)
        {
            var filters = new OTTUserFilter { UsernameEqual = username };
            var foundUser = FindUsers(filters).FirstOrDefault(u => u.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase));

            return foundUser;
        }

        public IEnumerable<OTTUser> FindUsers(OTTUserFilter filters)
        {
            ListResponse<OTTUser> ottUsersResult = null;

            try
            {
                ottUsersResult = OttUserService.List(filters).ExecuteAndWaitForResponse(_PhoenixClient);
            }
            catch (Exception ex)
            {
                var apiException = ex?.InnerException as APIException;
                if (apiException?.Code == APIException.UserDoesNotExist)
                {
                    return new List<OTTUser>();
                }
                else
                {
                    _Logger.Error($"Unexpected error while searching for users with UsernameEqual:{filters.UsernameEqual}, externalID:{filters.ExternalIdEqual}", ex);
                    throw;
                }
            }

            var foundUser = ottUsersResult.Objects;
            return foundUser;
        }

        public OTTUser UpdatedOttUser(OTTUser ottUser)
        {
            var ottUserToUpdate = new OTTUser
            {
                //Username = ottUser.Username,
                // TODO: getting error Kaltura.APIException: External ID already exists, until resolved will not update external ID
                //ExternalId = ottUser.ExternalId,
                Email = ottUser.Email,
                Phone = ottUser.Phone,
                DynamicData = ottUser.DynamicData,
            };

            var updatedOttUser = OttUserService.Update(ottUserToUpdate, ottUser.Id).ExecuteAndWaitForResponse(_PhoenixClient);
            return updatedOttUser;
        }

        
        public UserLoginPin GetLoginPinForUser(int userID)
        {
            _PhoenixClient.setUserId(int.MinValue);
            _PhoenixClient.setUserId(userID);
            var pin = UserLoginPinService.Add(_PhoenixClient.KS).ExecuteAndWaitForResponse(_PhoenixClient);
            _PhoenixClient.setUserId(int.MinValue);
            return pin;
        }

        public bool IsBrandIDofSetTopBoxType(int brandID)
        {
            var deviceBrands = DeviceBrandService.List().ExecuteAndWaitForResponse(_PhoenixClient);
            var deviceFamilyID = deviceBrands.Objects.FirstOrDefault(b => b.Id == brandID)?.DeviceFamilyid;
            return deviceFamilyID == 2;
        }

        private string GetPhoenixToken()
        {
            var cacheKeyForKS = $"Beeline_SSO_KS";
            var cache = MemoryCache.Default;

            _Logger.Info($"searching for KS in cache using key {cacheKeyForKS}");
            var ks = (string)cache[cacheKeyForKS];
            if (ks != null) { _Logger.Info("No KS in cache generating a new one"); }
            ks = ks ?? GenerateNewKS(cacheKeyForKS);
            return ks;
        }

        private string GenerateNewKS(string cacheKeyForKS)
        {
            var cache = MemoryCache.Default;
            _Logger.Info($"SignIn with app token.");
            var anonymousLoginResponse = OttUserService.AnonymousLogin(_GroupID).ExecuteAndWaitForResponse(_PhoenixClient);
            var anonymousKS = anonymousLoginResponse.Ks;
            var tokenToHash = anonymousKS + _AppToken;
            var tokenHash = Sha256Hash(tokenToHash);
            _PhoenixClient.setKS(anonymousKS);
            var loginResponse = AppTokenService.StartSession(_AppID, tokenHash).ExecuteAndWaitForResponse(_PhoenixClient);

            var policy = new CacheItemPolicy() { AbsoluteExpiration = DateTimeOffset.UtcNow.AddDays(5) };
            cache.Set(cacheKeyForKS, loginResponse.Ks, policy);
            return loginResponse.Ks;
        }

        public static string Sha256Hash(string value)
        {
            using (var hash = SHA256.Create())
            {
                return string.Concat(hash
                    .ComputeHash(Encoding.UTF8.GetBytes(value))
                    .Select(item => item.ToString("x2")));
            }
        }

        public static IDictionary<string, StringValue> ToStringValueDictionary(IDictionary<string, string> sourceDictionary)
        {
            return sourceDictionary?.ToDictionary(k => k.Key, v => new StringValue() { Value = v.Value });
        }
    }

    internal class PhoenixLoggerForLog4Net : ILogger
    {
        private static readonly ILog _Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void Log(string msg)
        {
            _Logger.Info(msg);
        }
    }
}